import React, {Component} from 'react'

import SideMenu from './components/SideMenu';

export default class Menu extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isToggleOn: false
        };

        this.handleClick = this.handleClick.bind(this);
        this.onBackClick = this.onBackClick.bind(this);
    }

    handleClick() {
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    onBackClick() {
        this.setState(() => ({
            isToggleOn: true
        }));
    }

    render() {
        return (
            <nav className="menu">

                <SideMenu open={this.state.isToggleOn} onBackClick={this.onBackClick}/>

                <div className="menu_logo">
                    <svg className="icon icon-logo">
                        <use xlinkHref="#icon-logo"></use>
                    </svg>
                </div>
                <div className="menu_link menu_link_calendar">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-1">
                            <use xlinkHref="#icon-menu-1"></use>
                        </svg>
                    </div>
                </div>
                <div className="menu_link">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-2">
                            <use xlinkHref="#icon-menu-2"></use>
                        </svg>
                    </div>
                </div>
                <div className="menu_link">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-3">
                            <use xlinkHref="#icon-menu-3"></use>
                        </svg>
                    </div>
                </div>
                <div className="menu_link">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-4">
                            <use xlinkHref="#icon-menu-4"></use>
                        </svg>
                    </div>
                </div>
                <div className="menu_link">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-5">
                            <use xlinkHref="#icon-menu-5"></use>
                        </svg>
                    </div>
                </div>
                <div onClick={this.handleClick} className="menu_link">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-6">
                            <use xlinkHref="#icon-menu-6"></use>
                        </svg>
                    </div>
                </div>
                <div className="menu_link menu_link_login">
                    <div className="menu_icon">
                        <svg className="icon icon-menu-7">
                            <use xlinkHref="#icon-menu-7"></use>
                        </svg>
                    </div>
                </div>
            </nav>

        );
    }
}

