import React from 'react'

import ToggleSwitch from './components/ToggleSwitch';

export default function Map() {
    return (
        <div className="map">
            <div className="map_panel">
                <ToggleSwitch/>
            </div>
            <div className="map_zoom">
                <div className="map_zoom_pros">
                    <svg className="icon icon-pros">
                        <use xlinkHref="#icon-pros"></use>
                    </svg>
                </div>
                <div className="map_zoom_cons"></div>
            </div>
            <div className="map_img" style={{backgroundImage: "url(img/map-1.png )"}}>
                <div className="map_info map_info--left">Вегетация, 25 мая 2019</div>
            </div>
            <div className="map_line"></div>
            <div className="map_img" style={{backgroundImage: "url(img/map-2.png )"}}>
                <div className="map_info map_info--right">Рельеф, SRTM</div>
            </div>
        </div>
    )
}
