import React from 'react';
import './App.scss';

import Icon from './Icon';
import Menu from './Menu';
import Map from './Map';

function App() {
    return (
        <div className="layout">
            <Icon/>
            <Menu/>
            <Map/>
        </div>
    );
}

export default App;
