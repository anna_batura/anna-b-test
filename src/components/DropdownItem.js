import React, {Component} from 'react'

export default class DropdownItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isToggleDropdown: false
        };

        this.openDropdown = this.openDropdown.bind(this);
    }

    openDropdown() {
        this.setState(state => ({
            isToggleDropdown: !state.isToggleDropdown
        }));
    }

    render() {
        return (

            <li key={this.props.id}
                className={"dropdown " + (this.props.disabled ? 'disabled' : '') + (this.state.isToggleDropdown ? ' open' : '')}>

                <div className="dropdown_icon dropdown_icon_copy">
                    <svg className="icon icon-copy">
                        <use xlinkHref="#icon-copy"></use>
                    </svg>
                </div>

                <div className="dropdown_icon dropdown_icon_delete">
                    <svg className="icon icon-delete">
                        <use xlinkHref="#icon-delete"></use>
                    </svg>
                </div>

                <div onClick={this.openDropdown} className="dropdown_top">
                    <div className="dropdown_icon_arrow">
                        <svg className="icon icon-arrow-seclect">
                            <use xlinkHref="#icon-arrow-seclect"></use>
                        </svg>
                    </div>
                    <div className="dropdown_icon_lock">
                        <div className="dropdown_tooltip">Можно сравнивать максимум 9 слоёв</div>
                        <svg className="icon icon-lock">
                            <use xlinkHref="#icon-lock"></use>
                        </svg>
                    </div>

                    <span className="dropdown_title">{this.props.title}
                        <span className="dropdown_info">{this.props.info}</span>
                <div className="dropdown_icon dropdown_icon_eye">
                    <svg className="icon icon-eye">
                        <use xlinkHref="#icon-eye"></use>
                    </svg>
                </div>
                </span>

                </div>

                <div className="dropdown_inner">
                    <this.props.inner/>
                </div>
            </li>

        );
    }
}
