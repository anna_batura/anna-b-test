import React from 'react'

export default function ToggleSwitch() {
    return (
        <div className="toggle-switch">
            <input className="toggle-switch_input" id="btn1" name="toggle-switch" type="checkbox"/>
            <label className="toggle-switch_background" htmlFor="btn1"/>
            <label className="toggle-switch_btn">Матрица</label>
            <label className="toggle-switch_btn">Шторка</label>
        </div>

    )
}


