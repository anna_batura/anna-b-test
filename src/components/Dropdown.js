import React from 'react'

import DropdownItem from './DropdownItem';

export default function Dropdown(props) {
    return (
        <div className="dropdowns">
            <ul>
                {props.layers.map((layer, index) =>
                    <DropdownItem {...layer} key={index}/>
                )}
            </ul>
            <button className="side-menu_btn-add">
                <svg className="icon icon-add side-menu_icon-add">
                    <use xlinkHref="#icon-add"></use>
                </svg>
                Добавить слой для сравнения
            </button>
        </div>
    );
}
