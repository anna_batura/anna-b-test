import React from 'react'
import Dropdown from './Dropdown';
import CalendarCustom from './CalendarCustom';
import SelectCustom from './SelectCustom';

export default function SideMenu(props) {
    return (
        <div className={"side-menu " + (props.open ? '' : 'active')}>
            <div className="side-menu_header">
                <div className="side-menu_arrow" onClick={props.onBackClick}>
                    <svg className="icon icon-arrow">
                        <use xlinkHref="#icon-arrow"></use>
                    </svg>
                </div>
                <div className="side-menu_title">192. Ждановичи, 17 га</div>
                <div className="side-menu_info">Пшеница</div>
            </div>
            <div className="side-menu_dropdowns">
                <Dropdown layers={layers}/>
            </div>
            <button className="btn side-menu_btn">
                <svg className="btn_icon icon icon-plus">
                    <use xlinkHref="#icon-plus"></use>
                </svg>
                Загрузить файл
            </button>
        </div>
    );
}

const layers = [
    {id: 1, title: 'Вегетация', info: '', disabled: false, inner: CalendarCustom},
    {id: 2, title: 'Рельеф', info: '', disabled: false, inner: SelectCustom},
    {id: 3, title: 'Посев', info: 'file-2018, Speed', disabled: false, inner: SelectCustom},
    {id: 4, title: 'Хим. анализ', info: '25 мая 2019', disabled: false, inner: SelectCustom},
    {id: 5, title: 'Посев', info: 'file-2018, Speed', disabled: true, inner: SelectCustom},
    {id: 6, title: 'Вегетация', info: '25 мая 2019', disabled: true, inner: SelectCustom},
    {id: 7, title: 'Рельеф', info: 'SRTM', disabled: true, inner: SelectCustom},
];