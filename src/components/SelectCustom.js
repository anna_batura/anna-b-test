import React from 'react'

export default function SelectCustom() {
    return (
        <div>
            <select className="select">
                <option>SRTM</option>
                <option>DDD</option>
            </select>
        </div>
    )
}
