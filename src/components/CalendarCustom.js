import React from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import 'moment/locale/ru';
import {
    formatDate,
    parseDate,
} from 'react-day-picker/moment';


const WEEKDAYS_SHORT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']

const MONTHS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
]

export default class CalendarCustom extends React.Component {
    render() {
        return (
            <div className="calendar">
                <DayPickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    format="LL"
                    placeholder={`${formatDate(new Date(), 'LL', 'ru')}`}
                    dayPickerProps={{
                        locale: "ru",
                        months: MONTHS,
                        weekdaysShort: WEEKDAYS_SHORT,
                        firstDayOfWeek: 1,
                        fixedWeeks: true
                    }}
                />
                <div className="calendar_counter">
                    <button className="calendar_btn calendar_btn_prev"></button>
                    <button className="calendar_btn calendar_btn_next"></button>
                </div>
            </div>
        );
    }
}